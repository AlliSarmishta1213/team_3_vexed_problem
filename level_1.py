import tkinter as tk
from tkinter import messagebox

root = tk.Tk()
root.title("Color Puzzle")

puzzle = [["#", 'A', '-', '-', '#'],
          ['#', '#', '-', 'B', '#'],
          ['#', 'A', 'B', '#', '#']]

NR = len(puzzle)
NC = len(puzzle[0])
colours = []
for i in range(NR):
    for j in range(NC):
        if puzzle[i][j] == "#":
            colour = "gray"
            colours.append(colour)

        elif puzzle[i][j] == "-":
            colour = "white"
            colours.append(colour)

        elif puzzle[i][j] == "A":
            colour = "red"
            colours.append(colour)

        elif puzzle[i][j] == "B":
            colour = "blue"
            colours.append(colour)

# Create a frame to hold the puzzle grid
puzzle_frame = tk.Frame(root)
puzzle_frame.pack(padx=10, pady=10)

# Create a list to store references to color buttons
color_buttons = []

def create_buttons():
    for i in range(NR):
        for j in range(NC):
            index = i * NC + j
            if index < len(colours):
                color = colours[index]
                button = tk.Button(puzzle_frame, bg=color, width=20, height=10, command=lambda idx=index: on_click(idx))
                button.grid(row=i, column=j, padx=1, pady=1)
                color_buttons.append(button)

create_buttons()

# Variables to store the clicked block and its index
selected_block = None
selected_index = None

moves = 0
successful = False

def on_click(index):
    global selected_block, selected_index, moves, successful
    if selected_block is None:
        if colours[index] in ["red", "blue"]:
            selected_block = colours[index]
            selected_index = index
    elif selected_block == colours[index]:
        if is_adjacent(selected_index, index):
            cancel_blocks(selected_index, index)
        selected_block = None
        selected_index = None

def cancel_blocks(idx1, idx2):
    puzzle_idx1 = divmod(idx1, NC)
    puzzle_idx2 = divmod(idx2, NC)
    puzzle[puzzle_idx1[0]][puzzle_idx1[1]] = puzzle[puzzle_idx2[0]][puzzle_idx2[1]] = '-'
    colours[idx1] = colours[idx2] = "white"
    update_puzzle_display()

def update_puzzle_display():
    for i in range(NR):
        for j in range(NC):
            index = i * NC + j
            if index < len(colours):
                color = colours[index]
                color_buttons[index].config(bg=color)

def on_key(event):
    global selected_block, selected_index, moves, successful

    if event.keysym in ['Up', 'Down', 'Left', 'Right']:
        dx, dy = 0, 0
        if event.keysym == 'Up':
            dx, dy = -1, 0
        elif event.keysym == 'Down':
            dx, dy = 1, 0
        elif event.keysym == 'Left':
            dx, dy = 0, -1
        elif event.keysym == 'Right':
            dx, dy = 0, 1

        if selected_block and selected_block in ["red", "blue"]:
            new_x, new_y = divmod(selected_index, NC)
            new_x += dx
            new_y += dy

            new_index = new_x * NC + new_y

            if (
                0 <= new_x < NR
                and 0 <= new_y < NC
                and colours[new_index] == "white"
                and is_adjacent(selected_index, new_index)
            ):
                moves += 1
                puzzle_idx1 = divmod(selected_index, NC)
                puzzle_idx2 = divmod(new_index, NC)
                puzzle[puzzle_idx1[0]][puzzle_idx1[1]] = puzzle[puzzle_idx2[0]][puzzle_idx2[1]] = "-"
                colours[selected_index] = "white"
                colours[new_index] = selected_block
                selected_block = None
                selected_index = None
                update_puzzle_display()

                if moves <= 11 and all(colour in ["red", "blue", "white"] for colour in colours):
                    successful = True
                    for i in range(NR):
                        for j in range(NC):
                            index = i * NC + j
                            if index < len(colours):
                                if colours[index] in ["red", "blue"]:
                                    adjacent_count = 0
                                    if i > 0 and colours[(i - 1) * NC + j] == colours[index]:
                                        adjacent_count += 1
                                    if i < NR - 1 and colours[(i + 1) * NC + j] == colours[index]:
                                        adjacent_count += 1
                                    if j > 0 and colours[i * NC + (j - 1)] == colours[index]:
                                        adjacent_count += 1
                                    if j < NC - 1 and colours[i * NC + (j + 1)] == colours[index]:
                                        adjacent_count += 1
                                    if adjacent_count >= 2:
                                        colours[index] = "white"
                    update_puzzle_display()
                    messagebox.showinfo("Success", "Congratulations! You completed the puzzle successfully in {} moves.".format(moves))

def is_adjacent(idx1, idx2):
    row1, col1 = divmod(idx1, NC)
    row2, col2 = divmod(idx2, NC)
    return abs(row1 - row2) + abs(col1 - col2) == 1

# Bind arrow key events to the on_key function
root.bind('<Up>', on_key)
root.bind('<Down>', on_key)
root.bind('<Left>', on_key)
root.bind('<Right>', on_key)

# Start the tkinter event loop
root.mainloop()
