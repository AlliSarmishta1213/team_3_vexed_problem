import tkinter as tk

root = tk.Tk()
root.title("Color Puzzle")

puzzle = [
    ["#", '-', '-', 'Y', "-", "-", '#'],
    ['#', '-', 'Z', 'X', "-", "X", '#'],
    ['#', '-', '#', '#', "-", "-", '#'],
    ["#", '-', 'W', 'Z', '-', '-', "#"],
    ['#', "#", "#", "#", 'Y', 'W', "#"]
]

NR = len(puzzle)
NC = len(puzzle[0])
colours = []

for i in range(NR):
    for j in range(NC):
        if puzzle[i][j] == "#":
            colour = "gray"
            colours.append(colour)
        elif puzzle[i][j] == "-":
            colour = "white"
            colours.append(colour)
        elif puzzle[i][j] == "X":
            colour = "red"
            colours.append(colour)
        elif puzzle[i][j] == "Y":
            colour = "blue"
            colours.append(colour)
        elif puzzle[i][j] == "Z":
            colour = "yellow"
            colours.append(colour)
        elif puzzle[i][j] == "W":
            colour = "green"
            colours.append(colour)


puzzle_frame = tk.Frame(root)
puzzle_frame.pack(padx=10, pady=10)

color_buttons = []

def create_buttons():
    for i in range(NR):
        for j in range(NC):
            index = i * NC + j
            if index < len(colours):
                color = colours[index]
                button = tk.Button(puzzle_frame, bg=color, width=15, height=7, command=lambda idx=index: on_click(idx))
                button.grid(row=i, column=j, padx=1, pady=1)
                color_buttons.append(button)

create_buttons()

selected_block = None
selected_index = None

def on_click(index):
    global selected_block, selected_index
    if colours[index] != "gray":  # Skip grey blocks
        if selected_block is None:
            selected_block = colours[index]
            selected_index = index
        else:
            if is_adjacent(selected_index, index) or is_l_shaped(selected_index, index):
                if selected_block == colours[index]:
                    if can_cancel_blocks(selected_index, index):
                        cancel_blocks(selected_index, index)
                        check_and_cancel_adjacent(selected_index)
                    selected_block = None
                    selected_index = None
                else:
                    move_block(selected_index, index)
                    selected_block = None
                    selected_index = None
            else:
                selected_block = colours[index]
                selected_index = index

def is_adjacent(idx1, idx2):
    row1, col1 = divmod(idx1, NC)
    row2, col2 = divmod(idx2, NC)
    return abs(row1 - row2) + abs(col1 - col2) == 1

def is_l_shaped(idx1, idx2):
    row1, col1 = divmod(idx1, NC)
    row2, col2 = divmod(idx2, NC)
    return abs(row1 - row2) == 1 and abs(col1 - col2) == 1

def can_cancel_blocks(idx1, idx2):
    return (is_adjacent(idx1, idx2) or is_l_shaped(idx1, idx2)) and selected_block == colours[idx2]

def cancel_blocks(idx1, idx2):
    puzzle_idx1 = divmod(idx1, NC)
    puzzle_idx2 = divmod(idx2, NC)
    puzzle[puzzle_idx1[0]][puzzle_idx1[1]] = puzzle[puzzle_idx2[0]][puzzle_idx2[1]] = '-'
    colours[idx1] = colours[idx2] = "white"
    update_puzzle_display()

def move_block(idx1, idx2):
    global colours, puzzle

    puzzle_idx1 = divmod(idx1, NC)
    puzzle_idx2 = divmod(idx2, NC)
    puzzle[puzzle_idx1[0]][puzzle_idx1[1]], puzzle[puzzle_idx2[0]][puzzle_idx2[1]] = \
        puzzle[puzzle_idx2[0]][puzzle_idx2[1]], puzzle[puzzle_idx1[0]][puzzle_idx1[1]]
    colours[idx1], colours[idx2] = colours[idx2], colours[idx1]
    update_puzzle_display()

def check_and_cancel_adjacent(index):
    row, col = divmod(index, NC)
    adjacent_indices = [index - NC, index + NC, index - 1, index + 1]

    for adj_idx in adjacent_indices:
        if 0 <= adj_idx < NR * NC:
            adj_row, adj_col = divmod(adj_idx, NC)
            if is_adjacent(index, adj_idx) or is_l_shaped(index, adj_idx):
                if colours[adj_idx] == selected_block:
                    cancel_blocks(adj_idx, index)

def update_puzzle_display():
    for i in range(NR):
        for j in range(NC):
            index = i * NC + j
            if index < len(colours):
                color = colours[index]
                color_buttons[index].config(bg=color)

def on_key(delta_row, delta_col):
    global selected_index
    new_row = selected_index // NC + delta_row
    new_col = selected_index % NC + delta_col

    new_index = new_row * NC + new_col

    if 0 <= new_row < NR and 0 <= new_col < NC:
        on_click(new_index)

root.bind('<Up>', lambda event: on_key(-1, 0))
root.bind('<Down>', lambda event: on_key(1, 0))
root.bind('<Left>', lambda event: on_key(0, -1))
root.bind('<Right>', lambda event: on_key(0, 1))

root.mainloop()
